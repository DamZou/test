var gulp = require('gulp');
var sass = require('gulp-sass');
var connect = require('gulp-connect');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var minifyHtml = require('gulp-minify-html');
var rename = require('gulp-rename');
var cleanCss = require('gulp-clean-css');
var bower = './app/bower_components';
var autoprefixer = require('gulp-autoprefixer');

gulp.task('connect', function(){
	connect.server({
		root: 'public',
		port: 4000
	});
});

gulp.task('browserify', function(){
	return browserify('./app/app.js')
		.bundle()
		.pipe(source('main.js'))
		.pipe(gulp.dest('./public/js/'));
});

gulp.task('miniHtml', function(){
	gulp.src('./app/views/*.html')
	.pipe(minifyHtml())
	.pipe(gulp.dest('./public/views')); 
});

gulp.task('watch', function(){
	gulp.watch('app/**/*.js', ['browserify']);
	gulp.watch('sass/main.scss', ['sass']);
	gulp.watch('app/views/*.html', ['miniHtml']);
});

gulp.task('sass', function(){
	return gulp.src('sass/main.scss')
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(rename('main.css'))
		.pipe(cleanCss())
		.pipe(gulp.dest('public/css'));
});

gulp.task('default', ['connect', 'watch']);

