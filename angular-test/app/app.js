require('angular');
require('angular-route');

var home = require('./controllers/home');
var register = require('./controllers/register');
var session = require('./controllers/session');
var login = require('./controllers/login');

var app = angular.module('app', ['ngRoute']);
app.config(['$routeProvider', '$locationProvider',
	function($routeProvider, $locationProvider){
	    $routeProvider
	    .when('/home', {
		templateUrl : 'views/home.html',
		controller : 'sessionCtrl'
	    })
	    .when('/register',{
	    templateUrl : 'views/register.html'
	    })
	    .when('/login',{
	    templateUrl : 'views/login.html'
	    })
	    .otherwise({
		template: "does not exists"
	    });
	    $locationProvider.html5Mode(true)
}]);

app.controller('homeCtrl', ['$scope', home]);
app.controller('registerCtrl', ['$scope', '$http', '$location', register]);
app.controller('loginCtrl', ['$scope', '$http', '$location', login]);
app.controller('sessionCtrl', ['$scope', session]);