module.exports = function($scope, $http, $location){
	$scope.loginUser = function(){
		var user = {
		email: $scope.email,
		password: $scope.password
		}
  		$http.post('http://localhost:3000/login', user)
  		.then(function(){
  			$location.path('home');
  		});
	}
}