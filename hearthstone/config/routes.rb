Rails.application.routes.draw do

	post '/register', to: 'users#register', defaults: { :format => 'json' }
	post '/login', to: 'users#login', defaults: { :format => 'json' }
	
end
