class User < ApplicationRecord

	has_secure_password

	validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, confirmation: true
  	validates :password, length: {minimum: 6}, allow_nil: true
  	validates :email, presence: true, uniqueness: { case_sensitive: false }
  	validates :name, uniqueness: { case_sensitive: true }
	
end
