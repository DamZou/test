class UsersController < ApplicationController

  def register
    binding.pry
    user = User.new(:name => params[:name],
                     :email => params[:email],
                     :password => params[:password],
                     :password_confirmation => params[:password_confirmation])
    if user.save
        render :json => ''
    else
        msg = {:error => "error"}
        render :json => msg
    end
  end

  def login
    user = User.find_by(email: params[:'email'].downcase)
    if user && user.authenticate(params[:password_digest])
      log_in user
      render :json => ''
    else
      render :json => ''
    end
  end

end
